use rusdis::rusdis_client::RusdisClient;
use rusdis::GetRequest;

pub mod rusdis {
    tonic::include_proto!("rusdis");
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    let mut client = RusdisClient::connect("http://[::1]:50051").await?;

    let request = tonic::Request::new(GetRequest {
        key: "my_test".into(),
    });

    let response = client.get(request).await?;

    println!("RESPONSE={:?}", response);

    Ok(())
}
